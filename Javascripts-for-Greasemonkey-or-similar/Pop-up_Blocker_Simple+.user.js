// ==UserScript==
// @name        Pop-up blocker Simple+
// @namespace   
// @description Simplest pop-up blocker ever!
// @run-at      document-start

// @include     *://bitsnoop.com/*
// @include     *://erpman1.tripod.com/*
// @include     *://fenopy.se/*
// @include     *://thepiratebay.se/*
// @include     *://torrentz.eu/*
// @include     *://*.4shared.com/*
// @include     *://*.angelfire.com/*
// @include     *://*.clipconverter.cc/*
// @include     *://*.filefactory.com/*
// @include     *://*.mediafire.com/download/*
// @include     *://*.minecraftprojects.net/*
// @include     *://*.optifine.net/*
// @include     *://*.optifined.net/*
// @include     *://*.pastehere.info/*
// @include     *://*.sendspace.com/*
// @include     *://*.wikia.com/*
// @include     *://*.youtube.com/*

// @grant       none
// @version     v1.0.0000
// ==/UserScript==
window.open = function(){};
