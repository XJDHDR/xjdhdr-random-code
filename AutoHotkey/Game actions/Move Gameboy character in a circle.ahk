; Recommended settings for performance
#NoEnv
ListLines, Off


#MaxThreads 2

#MaxThreadsPerHotkey 2
#IfWinActive, ahk_exe VisualBoyAdvance-M.exe, 
F11::
	Toggle := !Toggle
	Send, {Space down}
	While Toggle
	{
	; Put your custom code here
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}
		
		SetKeyDelay, , 1,

		IfWinNotActive, ahk_exe VisualBoyAdvance-M.exe, 
			WinActivate,  ahk_exe VisualBoyAdvance-M.exe, 
		Send, {s 2}
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		Sleep, 20

		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}
		
		IfWinNotActive, ahk_exe VisualBoyAdvance-M.exe, 
			WinActivate,  ahk_exe VisualBoyAdvance-M.exe, 
		Send, {d 2}
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		Sleep, 20
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		IfWinNotActive, ahk_exe VisualBoyAdvance-M.exe, 
			WinActivate,  ahk_exe VisualBoyAdvance-M.exe, 
		Send, {w 2}
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		Sleep, 20
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		IfWinNotActive, ahk_exe VisualBoyAdvance-M.exe, 
			WinActivate,  ahk_exe VisualBoyAdvance-M.exe, 
		Send, {a 2}
	
		If (Toggle <> 1)
		{
			SetKeyDelay, , 40,
			Send, {Space up}
			Send, {Space}
			Send, {Space}
			Exit
		}

		Sleep, 20
	; End of custom code
	}
Return
