; Recommended settings for performance
#NoEnv
ListLines, Off

; Any text to the right of a semicolon (;) is a comment and is ignored by AutoHotKey
; Modify essential settings
SetKeyDelay, , 100,
SetTitleMatchMode, 3
CoordMode, Mouse, Window
JC2StartProcedure:	; Create a label to jump here if Just Cause 2 is run again within 60 seconds
WinWaitActive, ahk_exe JustCause2.exe, ; Wait for JC2 to be started
WinGetPos, , , JC2WinWidth, JC2WinHeight, ahk_exe JustCause2.exe, 
AspectRatioOffset := JC2WinWidth / JC2WinHeight / 1.765567765567766
WinWaitClose, ahk_exe JustCause2.exe, ; Wait for JC2 to exit or crash
Loop {
	LoopStart:
	Sleep, 5000 ; Wait for 5 seconds
	If (TimeWaited > 60)
		ExitApp ; If I've waited for 60 seconds in total, stop the script from running.
	Else
	{
		IfWinActive, ahk_exe JustCause2.exe, ; Otherwise, check if JC2 was opened again in the last 5 seconds.
		{
			TimeWaited = 0 ; It was. Reset the 60 second timer and jump back to the beginning of the script.
			Goto, JC2StartProcedure
		}
		Else
		{
			TimeWaited += 5 ; It wasn't. Let's wait for another 5 seconds.
			Goto, LoopStart
		}
	}
}
ExitApp


#UseHook On  ; I need AutoHotKey to create a keyboard hook. JC2 blocks AHK's regular method of defining hotkeys.
#IfWinActive,  ahk_exe JustCause2.exe, ; Only hook F5 if the player has JC2 open and maximized.
F5::
Send, {Esc}  ; Open the main menu.
MouseGetPos, OriginalMouseX, OriginalMouseY, ; Get the mouse cursor's current position.
Sleep, 600
PositionMouseX := Round(JC2WinWidth / 510 * 95) ; Do some maths to calculate where "Save Game" option's X-coordinate is.
PositionMouseY := Round(JC2WinHeight / 290 * 110 / AspectRatioOffset) ; Do some maths to calculate where "Save Game" option's Y-coordinate is.
MouseMove, %PositionMouseX%, %PositionMouseY%, 3, ; Move mouse cursor to "Save Game" option
;MsgBox, %JC2WinWidth%`n%JC2WinHeight%`n%AspectRatioOffset%`n%PositionMouseX%`n%PositionMouseY%
Send, {Enter} ; Open the Save Game menu
Sleep, 300
;PositionMouseX := Round(JC2WinWidth / 510 * 90) ; Do some maths to calculate where the X-coordinate of the save slots are
;PositionMouseY := Round(A_ScreenWidth / 510 * 85) ; Do some maths to calculate where Slot 1's Y-coordinate is
;PositionMouseY := Round(JC2WinHeight / 290 * 200) ; Do some maths to calculate where Slot 10's Y-coordinate is
;MouseMove, %PositionMouseX%, %PositionMouseY%, 3, ; Move mouse cursor to Slot 1's position
Send, {Up} ; Select the 10th save slot
Sleep, 25
Send, {Enter} ; Overwrite the 10th save slot.
Sleep, 25
Send, {Up} ; Select Yes to confirm overwrite.
Sleep, 25
Send, {Enter} ; Accept my choice to overwrite the save game.
MouseMove, %OriginalMouseX%, %OriginalMouseY%, ; Move the mouse cursor back to it's original position.
Sleep, 7000 ; Wait 7 seconds for the game to finish saving.
Send, {Esc} ; Go back to the main menu.
Sleep, 25
Send, {Esc} ; Exit the main menu.
Return
